/**
 * Waypoints.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	autoCreatedAt: false,
	autoUpdatedAt: false,
	connection: 'mySqlServerRWDB',
	tableName: 'waypoints',
	attributes: {  
    idWaypoints: {
      type: 'INTEGER',
      columnName: "idwaypoints",
      primaryKey: true
    },
    idRobot: {
      type:'INTEGER',
      columnName: 'idRobot'
    },
    idMap: {
      type:'INTEGER',
      columnName: 'idMap'
    },
    wp_x: {
      type:'FLOAT',
      columnName: 'wp_x'
    },
    wp_y: {
      type:'FLOAT',
      columnName: 'wp_y'
    },
    wp_z: {
      type:'FLOAT',
      columnName: 'wp_z'
    },
    wpo_x: {
      type:'FLOAT',
      columnName: 'wpo_x'
    },
    wpo_y: {
      type:'FLOAT',
      columnName: 'wpo_y'
    },
    wpo_z: {
      type:'FLOAT',
      columnName: 'wpo_z'
    },
    wpo_w: {
      type:'FLOAT',
      columnName: 'wpo_w'
    }
  }
};

