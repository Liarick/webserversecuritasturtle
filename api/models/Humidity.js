/**
 * Humidity.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  connection: 'mySqlServerRWDB',
  tableName: 'humidity',
  attributes: {
		idHumidity:{
			type: 'INTEGER',
			columnName: 'idHumidity',
      primaryKey: true
		},
    value: {
      type: 'FLOAT',
      columnName: 'value'
    },
    day: {
      type: 'INTEGER',
      columnName: 'day'
    },
    month: {
      type: 'INTEGER',
      columnName: 'month'
    },
    year: {
      type: 'INTEGER',
      columnName: 'year'
    },
    hour: {
      type: 'INTEGER',
      columnName: 'hour'
    },
    minutes: {
      type:'INTEGER',
      columnName: 'minutes'
    },
    secondes: {
      type:'INTEGER',
      columnName: 'secondes'
    },
    idRobot: {
      type:'INTEGER',
      columnName: 'idRobot'
    }
    
  }
};

