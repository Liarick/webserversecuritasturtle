/**
 * Maps.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	autoCreatedAt: false,
	autoUpdatedAt: false,
	connection: 'mySqlServerRWDB',
	tableName: 'maps',
	attributes: {
		idMaps: {
			type: 'INTEGER',
			columnName: 'idMaps',
      		primaryKey: true
		},
		map_data: {
			type: 'STRING',
			columnName: 'map_data'
		},
		idRobot: {
			type: 'INTEGER',
			columnName: 'idRobot'
		},
		resolution: {
			type: 'FLOAT',
			columnName: 'resolution'
		},
		width: {
			type: 'INTEGER',
			columnName: 'width'
		},
		height: {
			type: 'INTEGER',
			columnName: 'height'
		},
		position: {
			type: 'STRING',
			columnName: 'position'
		},
		orientation: {
			type: 'STRING',
			columnName: 'orientation'
		}
		
	}
};

