/**
 * Robot.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  connection: 'mySqlServerRWDB',
  tableName: 'robot',
  attributes: {
    id: {
      type: 'INTEGER',
      columnName: 'id',
      primaryKey: true
    },
    name: {
      type: 'STRING',
      columnName: 'name'
    },
  }
};