/**
 * Sensor.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  connection: 'mySqlServerRWDB',
  tableName: 'sensor',
  attributes: {
    id: {
      type: 'INTEGER',
      columnName: 'id',
      primaryKey: true
    },
    value_temp: {
      type: 'FLOAT',
      columnName: 'value_temp'
    },
    value_hum: {
      type: 'FLOAT',
      columnName: 'value_hum'
    },
    value_felt: {
      type: 'FLOAT',
      columnName: 'value_felt'
    },
    date: {
      type: 'DATETIME',
      columnName: 'date'
    },
    idRobot: {
      type:'INTEGER',
      columnName: 'idRobot'
    }
  }
};