/**
 * TempHumController
 *
 * @description :: Server-side logic for managing view for temperature/humidity
 */

module.exports = {
  getTempHum: function (req, res) {
    Sensor.find({}, function(err, result){
        			res.view( 'temphum', {sensor: result} );
    });
  },
};