/**
 * WaypointsController
 *
 * @description :: Server-side logic for managing waypoints
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	add: function (req, res) {
		// test if the parameter idRobot is in the adress
		if (!req.param('idRobot')){
			res.status(404);
			res.view('404', {message: 'You need to specify the id of the robot'});
		}

		// Create a waypoint variable
		var waypoint = {wp_x:  req.param('wp_x'), 
						wp_y:  req.param('wp_y'), 
						wp_z:  req.param('wp_z'), 
						wpo_x: req.param('wpo_x'), 
						wpo_y: req.param('wpo_y'), 
						wpo_z: req.param('wpo_z'), 
						wpo_w: req.param('wpo_w'), 
						idMap: req.param('idMap'), 
						idRobot: req.param('idRobot')};
		// execute the waypoint to create it in the data base
		Waypoints.create(waypoint).exec(function(err,result){
			// SQL error management
			if(err){
				sails.log.debug('Some error occured' + err);
				return res.json(500, { error: 'Some error occured' });
			}
			// SQL request OK
			sails.log.debug('Success', JSON.stringify(result));
			return res.send('waypoint indication: x = ' + 
							req.param('wp_x')  + ' y = ' + 
							req.param('wp_y')  + ' z = ' +
							req.param('wp_z')  + ' ox = ' +
							req.param('wpo_x') + ' oy = ' +
							req.param('wpo_y') + ' oz = ' +
							req.param('wpo_z') + ' ow = ' +
							req.param('wpo_w') + ' ' +
							req.param('idRobot'));
		});
	},

	get: function (req, res){
		// test if the parameter idRobot is in the adress
		if (!req.param('idRobot')){
			res.status(404);
			res.view('404', {message: 'You need to specify the id of the robot'});
		}

		sails.log(req.param('idRobot'));


		Alerts.query('SELECT MAX(idMap) as idmap FROM waypoints WHERE waypoints.idRobot = ?', 
			[req.param('idRobot')], 
			function(err, rawResult){
				if (err) { return res.serverError(err); }
				sails.log.debug(rawResult);
				Alerts.query('SELECT * FROM waypoints WHERE waypoints.idMap = ?', 
					[rawResult[0].idmap], 
					function(err, rawResult){
						if (err) { return res.serverError(err); }

						Alerts.queryrawResult;

							
						return res.send(rawResult);
					});
			});
	},

	getWaypointsFromMaps: function (req, res) {
		if(!req.param('idMap'))
		{
			res.send(404, 'Sorry, we cannot find the map !');
		}
		else
		{
			var idMap = req.param('idMap');
			Maps.findOne({idMaps: idMap}, function(err, result) {
				Waypoints.query("SELECT * FROM waypoints w WHERE idMap = ? group by wp_x,wp_y", idMap, function(err, result2) {
					res.view('waypoints', {waypoints: result2, map: result});
				});
			});
		}
 	}
};

