/**
 * MapsController
 *
 * @description :: Server-side logic for managing maps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const fs = require('fs');

module.exports = {
	add: function (req, res) {
		// test if the parameter idRobot is in the adress
		if (!req.param('idRobot')){
			console.log("ERROR NO ID");
			res.status(404);
			return res.view('404', {message: 'You need to specify the id of the robot'});
		}

		var dt = new Date();
		id_map =   dt.getFullYear()     * 100000000 
				 + dt.getMonth()		* 1000000 
				 + dt.getDate()			* 10000
				 + dt.getHours()		* 100
				 + dt.getMinutes()		* 1;
		console.log(id_map);
  req.file('map').upload({
  	dirname: '/tmp/'
  }, function whenDone(err, uploadedFiles) {
	    if (err) {
	      return res.negotiate(err);
	    }

	    // If no files were uploaded, respond with an error.
	    if (uploadedFiles.length === 0){
	      return res.badRequest('No file was uploaded');
	    }

	    file = uploadedFiles[0].fd;

	    fs.readFile(file, 'utf8', function(err, data){
	    	if(err){
	    		res.status(404);
					return res.view('404', {message: 'You need to specify the id of the robot'});
	    	} else {
	    		var map_data = data.split(',');
	    		var metadata = req.body['meta_data'];
					// Create a map variable
					var map = {idMaps: id_map,
										 map_data    : JSON.stringify(map_data), 
										 resolution  : req.param('resolution'),
										 width       : req.param('width'),
										 height      : req.param('height'),
										 position    : req.param('position'),
										 orientation : req.param('orientation'),
										 idRobot     : req.param('idRobot')};

					
					// execute the map to create it in the data base
					Maps.create(map).exec(function(err,result){
						// SQL error management
						if(err){
							sails.log.debug('Some error occured' + err);
							return res.json(500, { error: 'Some error occured' });
						}
						// SQL request OK
						sails.log.debug('Success', JSON.stringify(result));
						return res.send({id:id_map});
					});
	    	}
	    })
			
	  });
	},

	getMaps: function (req, res) {
		Maps.find({}, function(err, result){
						res.view( 'maps', {maps: result} );
		});
 	}
};

