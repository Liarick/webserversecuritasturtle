 /**
 * AlertsController
 *
 * @description :: Server-side logic for managing alerts
 */

module.exports = {
  // API to insert alerts data into the data base
  add: function (req, res) {
    // test if the parameter name is in the adress
    if (!req.param('name')){
      res.status(404);
      res.view('404', {message: 'You need to specify the name of the alert'});
    }
    // test if the parameter date is in the adress
    if (!req.param('date')){
      res.status(404);
      res.view('404', {message: 'You need to specify the date of the alert'});
    }
    // test if the parameter idRobot is in the adress
    if (!req.param('idRobot')){
      res.status(404);
      res.view('404', {message: 'You need to specify the idRobot'});
    }
      

    // date should be YYYY-MM-DDTHH:mm:ss
    var date_alert = new Date(req.param('date'));
    
    // Create an alert variable
    var alert = {name: req.param('name'), day: date_alert.getDate(), 
                 month: date_alert.getMonth(), year: date_alert.getFullYear(), 
                 hour: date_alert.getHours(), minutes: date_alert.getMinutes(), 
                 secondes: date_alert.getSeconds(), idRobot: req.param('idRobot')};
    
    // execute the alert to create it in the data base
    Alerts.create(alert).exec(function(err,result){
      // SQL error management
      if(err){
        sails.log.debug('Some error occured' + err);
        return res.json(500, { error: 'Some error occured' });
      }
      // SQL request OK
      sails.log.debug('Success', JSON.stringify(result));
      return res.send('Alert raised: ' + req.param('name') + ' on ' + req.param('date'));
    });  
  
  },

  // API to get alerts data from the data base
  getAll: function (req, res) { 
    Alerts.query('SELECT * FROM alerts', ['id', 'name', 'day', 'month', 'year', 'hour', 'minutes', 'secondes', 'idRobot' ], function(err, rawResult){
      if (err) { return res.serverError(err); }

      sails.log(rawResult);

      return res.ok();
    });
    return res.send('Hi there!');
  },
};

